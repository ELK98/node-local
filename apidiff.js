const openapiDiff = require('openapi-diff');
const { resourceUsage } = require('process');
const sourceFile = require('./myfiles/f1.json');
const destinationFile = require('./myfiles/f2.json');
let compareResult;




async function CompareApi(){

    const result = await openapiDiff.diffSpecs({
        sourceSpec: {
          content: JSON.stringify(sourceFile),
          location: './myfiles/f1.json',
          format: 'openapi3'
        },
        destinationSpec: {
          content: JSON.stringify(destinationFile),
          location: './myfiles/f2.json',
          format: 'openapi3'
        }
      });
      

      if(result.breakingDifferencesFound){
        console.log("API breakingChangeFound");
        console.log(result.breakingDifferences);
      }
      else{
        console.log("API Breaking Change not Found");
        console.log(result.nonBreakingDifferences);
      }
      result.nonBreakingDifferences.forEach(element => {
        console.log(element);
      });
      //return result.nonBreakingDifferences.;
}

compareResult = CompareApi();
module.exports = { CompareApi, compareResult };